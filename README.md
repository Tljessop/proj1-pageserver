# Proj1_Simpler_Server
-------------
## Discription
  The start point and main body of this simple webserver was given by Prof. Ram Durairajan. Then extend to server the allowed files by Thomas Jessop
## Function of Program
  If a client request the allowed files of trivial.html or travial.css they will be servered as expected. Any file requested the is prepended with ~, //, or .. is forbidden.
## Authors
 ### Original files by Prof. Ram Durairajan
 ### Modified by Thomas Lynn Jessop
## Contact Information
  Thomas Jessop
  tjessop2@uoregon.edu